// When the user scrolls the page, execute myFunction
window.onscroll = function() {myFunction()};

// Get the header
var header = document.getElementById("the_main_menu");
var logo = document.getElementById("the_small_logo");

// Get the offset position of the navbar
var sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
    logo.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
    logo.classList.remove("sticky");
  }
} 


//relies on https://github.com/ariutta/svg-pan-zoom
//var svgElement = document.querySelector('#svg')
//var panZoomTiger = svgPanZoom(svgElement)