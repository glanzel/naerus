from wagtail.core.models import Page
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

class IndexPage(Page):
    class Meta:
        verbose_name = _('Index')
        abstract = True

    @property
    def childs(self):
        # Get list of child pages that are descendants of this page
        childs = self.get_children().live()
        # print("IndexPage getChilds:")
        # print(childs)
        # #childs = childs.order_by(
        # #    "-date"
        # #)
        return childs

    def get_context(self, request, *args, **kwargs):
        context = super(IndexPage, self).get_context(request, *args, **kwargs)
        childs = self.childs
        
        # Pagination
        page = request.GET.get('page')
        page_size = 16
        if hasattr(settings, 'BLOG_PAGINATION_PER_PAGE'):
            page_size = settings.BLOG_PAGINATION_PER_PAGE

        paginator = None
        if page_size is not None:
            paginator = Paginator(childs, page_size)  # Show 10 blogs per page
            try:
                childs = paginator.page(page)
            except PageNotAnInteger:
                childs = paginator.page(1)
            except EmptyPage:
                childs = paginator.page(paginator.num_pages)

        context['childs'] = childs
        context['paginator'] = paginator
        context['display'] = {}
        context['display']['rules'] = {"width":"1", "height":"1", "background":"2"}
        #context = get_blog_context(context)
        return context
    
