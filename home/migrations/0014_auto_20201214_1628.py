# Generated by Django 3.1.3 on 2020-12-14 16:28

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0013_menupage_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepage',
            name='body',
            field=wagtail.core.fields.StreamField([('heading', wagtail.core.blocks.CharBlock(form_classname='full title')), ('box1', wagtail.core.blocks.RichTextBlock()), ('image', wagtail.images.blocks.ImageChooserBlock(template='box1_image.html')), ('box2', wagtail.core.blocks.RichTextBlock()), ('box2_content_page', wagtail.core.blocks.PageChooserBlock(template='ContentPageBox.html')), ('three_news', wagtail.core.blocks.StructBlock([], template='three_news.html'))]),
        ),
    ]
