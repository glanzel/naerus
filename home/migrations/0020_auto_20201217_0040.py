# Generated by Django 3.1.3 on 2020-12-17 00:40

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0019_auto_20201216_2020'),
    ]

    operations = [
        migrations.AlterField(
            model_name='specialpage',
            name='body',
            field=wagtail.core.fields.StreamField([('heading', wagtail.core.blocks.CharBlock(form_classname='full title')), ('pageSummarize', wagtail.core.blocks.StructBlock([('width', wagtail.core.blocks.ChoiceBlock(choices=[(1, '1'), (2, '2'), (3, '3')], icon='cup')), ('height', wagtail.core.blocks.ChoiceBlock(choices=[(1, '1'), (2, '2')], icon='cup')), ('background', wagtail.core.blocks.ChoiceBlock(choices=[(2, 'image'), (1, 'color'), (0, 'no')], icon='cup')), ('sum_page', wagtail.core.blocks.PageChooserBlock())])), ('box1', wagtail.core.blocks.RichTextBlock()), ('image', wagtail.images.blocks.ImageChooserBlock(template='box1_image.html')), ('box2', wagtail.core.blocks.RichTextBlock()), ('box2_content_page', wagtail.core.blocks.PageChooserBlock(template='ContentPageBox.html')), ('three_news', wagtail.core.blocks.StructBlock([], template='three_news.html'))]),
        ),
    ]
