from django.db import models
from django.utils.translation import ugettext_lazy as _
from wagtail.core import blocks
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel, FieldRowPanel, InlinePanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.embeds.blocks import EmbedBlock
from wagtail.images import get_image_model_string
from django.db.models.fields import CharField
from django.conf import settings
#from blog.models import BlogPage
import datetime
from  home.abstract import IndexPage
from django.forms.fields import ChoiceField
from wagtail.images.edit_handlers import ImageChooserPanel
from builtins import getattr
from wagtailmedia.blocks import AbstractMediaChooserBlock
from django.utils.html import format_html, format_html_join
from django.forms.utils import flatatt
from wagtail_svgmap.blocks import ImageMapBlock
from wagtail.search import index


# Content Blocks

class TheMediaBlock(AbstractMediaChooserBlock):
    def render_basic(self, value, context=None):
        if not value:
            return ''

        if value.type == 'video':
            player_code = '''
            <div class="content-area width2 ">
                <video controls>
                    {0}
                    Your browser does not support the video tag.
                </video>
            </div>
            '''
        else:
            player_code = '''
            <div class="content-area width2 height1">
                <audio controls>
                    {0}
                    Your browser does not support the audio element.
                </audio>
            </div>
            '''

        return format_html(player_code, format_html_join(
            '\n', "<source{0}>",
            [[flatatt(s)] for s in value.sources]
        ))



# Basic Pages

class AppPage(Page):
    date = models.DateField(
        "Post date", default=datetime.datetime.today,
        help_text=_("For Future use as Event only.")
    )
    teaser = CharField(blank=True, max_length=300, verbose_name="A TEASER")   
    class Meta:
        abstract = True
    
    settings_panels = Page.settings_panels + [
        FieldPanel('date')
        ]
    
    def getSubmenu(self):
        return self.get_children().filter(live=True, show_in_menus=True)

class ContentPage(AppPage):

    image = models.ForeignKey(
        get_image_model_string(),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name=_('Header image')
    )
    
    body = StreamField([('content_image', ImageChooserBlock(template="cp_blocks/block_box2_image.html")),
                        ('w2_content_paragraph', blocks.RichTextBlock(template="cp_blocks/w2_content_paragraph.html",features=['h2', 'h3', 'h4', 'bold', 'italic', 'ol', 'ul', 'document-link', 'link'])),
                        ("embedVideo",  EmbedBlock(template="cp_blocks/w2_video.html")),                        
                        ('video', TheMediaBlock(icon='media')),
                        ("rawhtml", blocks.RawHTMLBlock()),
                        ])

    content_panels = Page.content_panels + [
        ImageChooserPanel('image'),
        FieldPanel('teaser'),
        StreamFieldPanel('body', classname="full"),
    ]
    
    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]


# Blocks

class LinkStructValue(blocks.StructValue):
    def url(self):
        external_url = self.get('external_url')
        page = self.get('page')
        if external_url:
            return external_url
        elif page:
            return page.url


class QuickLinkBlock(blocks.StructBlock):
    page = blocks.PageChooserBlock(label="page", required=False)
    external_url = blocks.URLBlock(label="external URL", required=False)

    class Meta:
        icon = 'site'
        value_class = LinkStructValue


#subblock for other strut blocks
class CssRulesBlock(blocks.StructBlock):
    width = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2'), (3, '3')], classname="short_dropdown")
    height = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2')],  form_classname="short_dropdown")
    background = blocks.ChoiceBlock(choices=[(2, "image"), (1, "color"), (0, "no")],  form_classname="short_dropdown")
    class Meta:
        form_classname="css_rules_block"

#subblock for latest Child strut blocks
class RulesBlock(blocks.StructBlock):
    width = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2'), (3, '3')], classname="short_dropdown")
    number = blocks.ChoiceBlock(choices=[(0, '0') ,(1, '1'), (2, '2')], icon='cup')
    background = blocks.ChoiceBlock(choices=[(2, "image"), (1, "color"), (0, "no")],  form_classname="short_dropdown")
    class Meta:
        form_classname="css_rules_block"

class LatestChildBlock(blocks.StructBlock):
    sum_page = blocks.PageChooserBlock()
    rules = RulesBlock(label="Rules")
    class Meta:
        template = "home_blocks/latest_child_block.html"

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        live_childs = value['sum_page'].specific.get_children().live()
        
        try: context["item"] = live_childs[int(value['rules']['number'])]
        except: context["item"] = "empty"
        print(context["item"]) 
        return context

class SummarizePageBlock(blocks.StructBlock):
    sum_page = blocks.PageChooserBlock(classname="long_dropdown", target_model=ContentPage)
    css_rules = CssRulesBlock(label="Rules")
    class Meta:
        template = "home_blocks/page_summarize_block.html"
        form_classname="short_dropdowns"
    
    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        return context

class ContentBlock(blocks.StructBlock):
    heading = blocks.CharBlock(form_classname="full title")
    text =  blocks.RichTextBlock(required=False)
    image = ImageChooserBlock(required=False)

    link = blocks.PageChooserBlock(label="TheLink",required=False, null=True)

    width = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2'), (3, '3')], icon='cup')
    height = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2')], icon='cup')
    background = blocks.ChoiceBlock(choices=[(1, "color"), (0, "no")], icon='cup')
    class Meta:
        template = "home_blocks/multi_content_block.html"
        label = "ContentBlock"

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        return context


    
    
class MenuPage(Page):
    type = models.CharField(
        choices=[
            ('footermenu', 'footermenu'),
            ('menu', 'menu')
        ],
        default='menu',
        blank=True,
        max_length=50,
    )

    settings_panels = Page.settings_panels + [
        FieldPanel('type')
    ]
    
    def url(self, request=None, current_site=None):
        return "#"

        
class HomePage(AppPage):
    body = StreamField([('latestChildBlock', LatestChildBlock(label="latest Sub-item")),
                        ('pageSummarize', SummarizePageBlock()),
                        ('multiContent', ContentBlock()),
                        ('documentBlock', DocumentChooserBlock(template="home_blocks/document_block.html")),
                        ('map', ImageMapBlock(template="image_map.html")),
                        ("rawhtml", blocks.RawHTMLBlock()),
                        ], blank=True, null=True)

    template_string = models.CharField(max_length=255, choices=(
                                         ("home/home_page.html", "Default Template"), 
                                         ("home/home_page_with_credits.html", "Three Column Template")), default = "home/home_page.html", verbose_name= "Template")
        
    @property
    def template(self):
        return self.template_string

    content_panels = Page.content_panels + [
        FieldPanel('teaser'),
        StreamFieldPanel('body', classname="full"),
    ]
    
    settings_panels = Page.settings_panels + [
        FieldPanel('template_string')
    ]


class MapPage(AppPage):
    body = StreamField([('map', ImageMapBlock(template="image_map.html")),
                        ("rawhtml", blocks.RawHTMLBlock()),
                        ])

    content_panels = Page.content_panels + [
        FieldPanel('teaser'),
        StreamFieldPanel('body', classname="full"),
    ]


 
#===============================================================================
# class SpecialPage(Page):
#     body = StreamField([('latestChildBlock', LatestChildBlock(label="latest Sub-item")),
#                         ('pageSummarize', SummarizePageBlock()),
#                         ('multiContent', ContentBlock()),
#                         ('documentBlock', DocumentChooserBlock(template="home_blocks/document_block.html")),
#                         ('map', ImageMapBlock(template="image_map.html")),
#                         #('box1', blocks.RichTextBlock()),
#                         #('image', ImageChooserBlock(template="box1_image.html")),
#                         #('box2', blocks.RichTextBlock()),
#                         #('box2_content_page', blocks.PageChooserBlock(template="ContentPageBox.html")),
#                         #('three_news', LatestPostsBlock(template="three_news.html")),
#                         ])
# 
#     content_panels = Page.content_panels + [
#         StreamFieldPanel('body', classname="full"),
#     ]
#===============================================================================

    
class ConferenceIndexPage(IndexPage):
    class Meta:
        verbose_name = _('ConferenceIndex')

    def get_template(self, request):
        return 'home/index_page.html'

    subpage_types = ['ContentPage', 'HomePage']


class BlogIndexPage(IndexPage):
    class Meta:
        verbose_name = _('BaseIndex')
    
    @property
    def childs(self):
        # Get list of child pages that are descendants of this page
        childs = self.get_children().live().specific()
        #=======================================================================
        # print("BlogIndexPage getChilds:")
        # print(childs)
        # for child in childs:
        #     print(child.specific.date)
        # #childs = childs.order_by("date")
        # childs = sorted(
        #     childs,
        #     key=lambda p: getattr(p, 'date') or getattr(p, 'last_published_at'),
        #     reverse=True
        # )
        #=======================================================================
        return childs


    def get_template(self, request):
        return 'home/index_page.html'

    subpage_types = ['ContentPage']
    
    
       

