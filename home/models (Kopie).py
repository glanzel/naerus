from django.db import models
from django.utils.translation import ugettext_lazy as _
from wagtail.core import blocks
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel, FieldRowPanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.embeds.blocks import EmbedBlock
from wagtail.images import get_image_model_string
from django.db.models.fields import CharField
from django.conf import settings
from blog.models import BlogPage
import datetime
from  home.abstract import IndexPage
from django.forms.fields import ChoiceField
from wagtail.images.edit_handlers import ImageChooserPanel
from builtins import getattr
from wagtailmedia.blocks import AbstractMediaChooserBlock
from django.utils.html import format_html, format_html_join
from django.forms.utils import flatatt
from wagtail_svgmap.blocks import ImageMapBlock



#Todo: Put Blocks somewhere else ?
class LatestPostsBlock(blocks.StructBlock):
        def get_context(self, value, parent_context=None):
            context = super().get_context(value, parent_context=parent_context)
            context['posts'] = BlogPage.objects.live().order_by('-date')[:3]
            return context
        
        def posts(self):
            blogs = BlogPage.objects.live().order_by('-date')[:3]
            return blogs

class LatestChildBlock(blocks.StructBlock):
    sum_page = blocks.PageChooserBlock()
    number = blocks.ChoiceBlock(choices=[(0, '0') ,(1, '1'), (2, '2')], icon='cup')
    width = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2')], icon='cup')
    #height = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2')], icon='cup')
    background = blocks.ChoiceBlock(choices=[(2, "image"), (1, "color"), (0, "no")], icon='cup')
    class Meta:
        template = "home_blocks/latest_child_block.html"

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        live_childs = value['sum_page'].specific.get_children().live().order_by('-last_published_at')
        try: context["item"] = live_childs[int(value['number'])]
        except: context["item"] = "empty"
        return context


class SummarizePageBlock(blocks.StructBlock):
    sum_page = blocks.PageChooserBlock(classname="long_dropdown")
    width = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2'), (3, '3')], classname="short_dropdown")
    height = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2')],  form_classname="short_dropdown")
    background = blocks.ChoiceBlock(choices=[(2, "image"), (1, "color"), (0, "no")],  form_classname="short_dropdown")
    class Meta:
        template = "home_blocks/page_summarize_block.html"
        form_classname="short_dropdowns"
    
    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        return context

class CssRulesBlock(blocks.StructBlock):
    width = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2'), (3, '3')], classname="short_dropdown")
    height = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2')],  form_classname="short_dropdown")
    background = blocks.ChoiceBlock(choices=[(2, "image"), (1, "color"), (0, "no")],  form_classname="short_dropdown")
    class Meta:
        form_classname="css_rules_block"

class RulesBlock(blocks.StructBlock):
    width = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2'), (3, '3')], classname="short_dropdown")
    number = blocks.ChoiceBlock(choices=[(0, '0') ,(1, '1'), (2, '2')], icon='cup')
    background = blocks.ChoiceBlock(choices=[(2, "image"), (1, "color"), (0, "no")],  form_classname="short_dropdown")
    class Meta:
        form_classname="css_rules_block"


class SummarizePageBlock2x2(blocks.StructBlock):
    sum_page = blocks.PageChooserBlock(label="Reference Page")
    css_rules = CssRulesBlock(label="Rules")
    
    def __init__(self, required=True, help_text=None, width=2, height=2, background = 2, **kwargs):
        self.width = width
        self.height = height
        self.background = background
        super().__init__(**kwargs)

    class Meta:
        template = "home_blocks/page_summarize_block.html"

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)        
        context['value']['width'] = self.width;
        context['value']['height'] = self.height;
        context['value']['background'] = self.background;
        
        return context



class ContentBlock(blocks.StructBlock):
    heading = blocks.CharBlock(form_classname="full title")
    text =  blocks.RichTextBlock()
    image = ImageChooserBlock(required=False)
    link = blocks.CharBlock()
    width = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2'), (3, '3')], icon='cup')
    height = blocks.ChoiceBlock(choices=[(1, '1'), (2, '2')], icon='cup')
    background = blocks.ChoiceBlock(choices=[(2, "image"), (1, "color"), (0, "no")], icon='cup')
    class Meta:
        template = "home_blocks/multi_content_block.html"


    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        return context
    
class TheMediaBlock(AbstractMediaChooserBlock):
    def render_basic(self, value, context=None):
        if not value:
            return ''

        if value.type == 'video':
            player_code = '''
            <div class="content-area width2 ">
                <video controls>
                    {0}
                    Your browser does not support the video tag.
                </video>
            </div>
            '''
        else:
            player_code = '''
            <div class="content-area width2 height1">
                <audio controls>
                    {0}
                    Your browser does not support the audio element.
                </audio>
            </div>
            '''

        return format_html(player_code, format_html_join(
            '\n', "<source{0}>",
            [[flatatt(s)] for s in value.sources]
        ))
    



class AppPage(Page):
    date = models.DateField(
        "Post date", default=datetime.datetime.today,
        help_text=_("Used mainly to order Pages.")
    )
    class Meta:
        abstract = True
    
    settings_panels = Page.settings_panels + [
        FieldPanel('date')
        ]
    
    def getSubmenu(self):
        return self.get_children().filter(live=True, show_in_menus=True)
    
class MenuPage(Page):
    type = models.CharField(
        choices=[
            ('footermenu', 'footermenu'),
            ('menu', 'menu')
        ],
        default='menu',
        blank=True,
        max_length=50,
    )

    settings_panels = Page.settings_panels + [
        FieldPanel('type')
    ]
        
class HomePage(Page):
    body = StreamField([('heading', blocks.CharBlock(form_classname="full title")),
                        ('box1', blocks.RichTextBlock()),
                        ('image', ImageChooserBlock(template="box1_image.html")),
                        ('box2', blocks.RichTextBlock()),
                        ('box2_content_page', blocks.PageChooserBlock(template="ContentPageBox.html")),
                        ('three_news', LatestPostsBlock(template="three_news.html")),
                        ])

    content_panels = Page.content_panels + [
        StreamFieldPanel('body', classname="full"),
    ]
 
class SpecialPage(Page):
    body = StreamField([('latestChildBlock', LatestChildBlock(label="latest Sub-item")),

                        ('pageSummarize2x2', SummarizePageBlock2x2(group="PageSummarize")),
                        ('pageSummarize', SummarizePageBlock(group="PageSummarize")),
                        ('multiContent', ContentBlock()),
                        ('map', ImageMapBlock(template="image_map.html")),
                        #('box1', blocks.RichTextBlock()),
                        #('image', ImageChooserBlock(template="box1_image.html")),
                        #('box2', blocks.RichTextBlock()),
                        #('box2_content_page', blocks.PageChooserBlock(template="ContentPageBox.html")),
                        #('three_news', LatestPostsBlock(template="three_news.html")),
                        ])

    content_panels = Page.content_panels + [
        StreamFieldPanel('body', classname="full"),
    ]

class ContentPage(AppPage):
    image = models.ForeignKey(
        get_image_model_string(),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name=_('Header image')
    )
    
    teaser = CharField(blank=True, max_length=300)   
    body = StreamField([('content_image', ImageChooserBlock(template="cp_blocks/block_box2_image.html")),
                        ('w2_content_paragraph', blocks.RichTextBlock(template="cp_blocks/w2_content_paragraph.html")),
                        ("embedVideo",  EmbedBlock(template="cp_blocks/w2_video.html")),                        
                        ('video', TheMediaBlock(icon='media')),
                        ("rawhtml", blocks.RawHTMLBlock()),
                        ])

    content_panels = Page.content_panels + [
        ImageChooserPanel('image'),
        FieldPanel('teaser'),
        StreamFieldPanel('body', classname="full"),
    ]
    
class ConferenceIndexPage(IndexPage):
    class Meta:
        verbose_name = _('ConferenceIndex')

    subpage_types = ['ContentPage']
        

