from django.db import models
from wagtail.core import blocks
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.images.blocks import ImageChooserBlock

class HomePage(Page):

    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('body', classname="full"),
    ]


#     def get_context(self, request, *args, **kwargs):
#         context = super(HomePage, self).get_context(request, *args, **kwargs)
#     
#         context['menuitems'] = self.get_site().root_page.get_children().filter(
#             live=True, show_in_menus=True)
#     
#         return context
    


class SpecialPage(Page):
    body = StreamField([('heading', blocks.CharBlock(form_classname="full title")),
                        ('box360', blocks.RichTextBlock()),
                        ('image', ImageChooserBlock(template="box1_image.html")),
                        ('box760', blocks.RichTextBlock()),
                        ('box1_content_page', blocks.PageChooserBlock(template="ContentPageBox.html")),
                        ])

    content_panels = Page.content_panels + [
        StreamFieldPanel('body', classname="full"),
    ]

        