from django import template
from wagtail.core.models import Page
from  home.models import MenuPage


register = template.Library()

@register.simple_tag
def footer_menu():
    footer = MenuPage.objects.live().filter(type="footermenu").first()
    print("Footer Template Tag")
    print(footer)
    if footer != None:
        footerpages = footer.get_children().live().in_menu()
    else:
        footerpages = None
    return footerpages

@register.inclusion_tag('tags/breadcrumbs.html', takes_context=True)
def breadcrumbs(context):
    self = context.get('self')
    if self is None or self.depth <= 2:
        # When on the home page, displaying breadcrumbs is irrelevant.
        ancestors = ()
    else:
        ancestors = Page.objects.ancestor_of(
            self, inclusive=True).filter(depth__gt=1)
    return {
        'ancestors': ancestors,
        'request': context['request'],
    }