from django.conf.urls import *
from django.urls import path
from . import views

urlpatterns = (

    path('hallo/', views.hallo, name='hallo'),
    path('sendtest/', views.sendtest, name='sendtest'),

)
