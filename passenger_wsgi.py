import sys, os
#print("Python version")
#print (sys.version)
ApplicationDirectory = 'naerus_site'
ApplicationName = 'naerus_site'
sys.path.append(os.path.join(sys.path[0], 'env', 'lib', 'python3.7', 'site-packages'))  
#sys.path.append("/var/www/py/naerus/env/lib/python3.7/site-packages")
#sys.path.pop(5)
#sys.path.pop(5)
#sys.path.pop(5)
#sys.path.pop(5)
print(sys.path)
os.chdir(os.path.join(os.getcwd(), ApplicationDirectory))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'naerus_site.settings.production')
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

