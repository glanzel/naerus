from wagtail.contrib.modeladmin.options import modeladmin_register

from .image_maps import ImageMapModelAdmin
#from .regions import RegionModelAdmin
from .map_group_admin import MapGroup 


def register():
    modeladmin_register(MapGroup)
    #modeladmin_register(RegionModelAdmin)
