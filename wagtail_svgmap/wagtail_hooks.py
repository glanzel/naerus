from django.conf import settings
from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register, ModelAdminGroup
from wagtail_svgmap.models import ImageMap, Region, City
from wagtail_svgmap.admin import ImageMapAdmin

if 'wagtail.contrib.modeladmin' in settings.INSTALLED_APPS:  # pragma: no cover
    import wagtail_svgmap.modeladmin as ma
    ma.register()



