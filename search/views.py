from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.template.response import TemplateResponse

from wagtail.core.models import Page
from wagtail.search.models import Query
from wagtail.search.backends import get_search_backend
from home.models import ContentPage, AppPage


def search(request):
    search_query = request.GET.get('query', None)
    page = request.GET.get('page', 1)

    # Search
    if search_query:
        print("search/views.py: search_query: "+search_query)
        #search_results = Page.objects.live().filter(contentpage__body__contains=search_query);
        #print(search_results)
        #s = get_search_backend()
        #result = s.search("Great", ContentPage, fields=['title', 'body'], operator='or')
        #print(result)
        search_results = ContentPage.objects.live().search(search_query, fields=['title', 'body'], operator='or')
        #print(search_results)
        query = Query.get(search_query)
        #print(query)
        

        # Record hit
        query.add_hit()
    else:
        search_results = Page.objects.none()

    # Pagination
    paginator = Paginator(search_results, 10)
    try:
        search_results = paginator.page(page)
    except PageNotAnInteger:
        search_results = paginator.page(1)
    except EmptyPage:
        search_results = paginator.page(paginator.num_pages)

    return TemplateResponse(request, 'search/search.html', {
        'search_query': search_query,
        'search_results': search_results,
    })
