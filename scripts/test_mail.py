from django.core.mail import send_mail

def run(*args):
    print("try sending mail")
    send_mail(
        'Test Subject',
        'Here is the message.',
        'travis.rakkr@gmail.com',
        ['grischan+9z5@gmail.com'],
        fail_silently=False,
    )
