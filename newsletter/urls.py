from birdsong import urls as birdsong_urls  
from django.urls import path, include
from . import views


urlpatterns = [
    path('subscribe', views.subscribe, name="subscribe"),
    path('edit/<uid>/<token>/',views.edit, name='edit'),
    path('mail/', include(birdsong_urls)),
]
