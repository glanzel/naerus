from django import forms
from .models import ExtendedContact

class SubscribeForm(forms.ModelForm):
    class Meta:
        model = ExtendedContact
        fields = ('email',);


class EditForm(forms.ModelForm):
    class Meta:
        model = ExtendedContact
        fields = ( 'email', 'first_name', 'last_name' );
