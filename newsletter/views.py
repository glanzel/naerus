from django.utils import timezone
from .models import ExtendedContact, SaleCampaign, ACampaign, Group
from newsletter.forms import SubscribeForm, EditForm
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.core.mail import send_mail
from django.template.loader import render_to_string # new
from django.core.mail import EmailMessage
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator

def subscribe(request, template='subscribe.html'):
    d = {}
    d['form'] = SubscribeForm()
    if request.method == 'POST':
        form = SubscribeForm(request.POST)
        if form.is_valid():
            item = form.save()
            
            # moved to edit
            #group = Group.objects.all().filter(name="default").first();
            #print(group.__dict__)
            #print(item.__dict__)
            #group.contact_set.add(item)
            #item.group.add(group)
            #item.last_login = timezone.now()
            #item.save()
            #logger.info(""+item+" wurde erfolgreich angelegt.")
            
            messages.success(request, 'Your Email got subscribed sucessfully. Please verify you Email Address. More Information: See Email')
            current_site = get_current_site(request)
            context = {}
            context['email'] = item.email
            context['user'] = item
            context['pk'] = item.pk
            context['domain'] = current_site.domain
            context['uid'] = urlsafe_base64_encode(force_bytes(item.pk))
            context['token'] = default_token_generator.make_token(item)
            
            html_message = render_to_string(
                template_name='mail/subscribe_email_message.html', context=context
            )

            print(html_message)
            email = EmailMessage(
                subject = 'Your Email got subscribed.',
                body = html_message,
                to = [item.email],
            ) 
            email.content_subtype = "html" # this is the crucial part 
            email.send()                       
            
            d['form'] = form
            next = request.POST.get('next', '/')
            return redirect(next)            
            #return render(request, template, d)
        else:
            messages.error(request, 'That didn`t work. Your account was not created.')
            return redirect('/')
    return render(request, template, d)


def edit(request, uid, token, template='edit_subscriber.html'):
    #print("AHAHAHAAHAH")
    pk = force_text(urlsafe_base64_decode(uid))
    print(pk)
    contact = get_object_or_404(ExtendedContact, pk=pk)
    print(contact)
    d = {}
    d['form'] = EditForm(instance=contact)
    d['pk'] = pk
    d['uid'] = uid
    d['token'] = token
    #print(d['form'])
    if request.method == 'POST':
        form = EditForm(request.POST, instance=contact)
        if form.is_valid():
            item = form.save()
            print(item)
            messages.success(request, 'Your data got saved.')
            d['form'] = form
            next = request.POST.get('next', '/')
            return redirect(next)            
        else:
            messages.error(request, 'That didn`t work.')
            return redirect('/')
    else :
        group = Group.objects.all().filter(name="default").first();
        #print(group.__dict__)
        #print(item.__dict__)
        #group.contact_set.add(item)
        contact.group.add(group)
        contact.last_login = timezone.now()
        contact.save()
        messages.success(request, 'Your Email got verified successfully.')

    return render(request, template, d)