from django_filters import FilterSet
from django_filters.filters import ModelChoiceFilter, ModelMultipleChoiceFilter

from .models import ExtendedContact, Group


class ContactFilter(FilterSet):
    group = ModelChoiceFilter(queryset=Group.objects.all())

    class Meta:
        model = ExtendedContact
        fields = ('group',)
        #print("ContaCTfILTER HAHAHAHA")
