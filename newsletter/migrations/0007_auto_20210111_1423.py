# Generated by Django 3.1.3 on 2021-01-11 14:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0006_auto_20210111_1418'),
    ]

    operations = [
        migrations.AlterField(
            model_name='extendedcontact',
            name='password',
            field=models.CharField(default='pbkdf2_sha256$216000$MCSJEkI25YMP$X6dpSjqVFF6IuvuegqqtDMSwJrmRlGKynjIEiiL06T0=', max_length=255),
        ),
    ]
