from django.utils import timezone
from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, InlinePanel
from wagtail.core.fields import StreamField
from django.contrib.auth.hashers import make_password
from birdsong.blocks import DefaultBlocks
from birdsong.models import Campaign, Contact
from django.forms import CheckboxSelectMultiple
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from django.contrib.sites.shortcuts import get_current_site

class SaleCampaign(Campaign):
    body = StreamField(DefaultBlocks())

    panels = Campaign.panels + [
        StreamFieldPanel('body'),
    ]

class ACampaign(Campaign):
    body = StreamField(DefaultBlocks())

    panels = Campaign.panels + [
        StreamFieldPanel('body'),
    ]
    
    def serve(self, request, *args, **kwargs):
        print("THIS IS serve of ACampaign")
        #respond = super().serve(request, *args, **kwargs)
        print("THIS IS still server of ACampaign")
        #return respond
    
class Group(models.Model):
    name = models.CharField(max_length=255)
    #type = models.CharField(max_length=255, choices=[('default','default'), ('other','other')])
    panels = [
        FieldPanel('name'),
        #FieldPanel('type', widget= CheckboxSelectMultiple),
    ]
    def __str__(self):
        return self.name

class ExtendedContact(Contact):
    group = ParentalManyToManyField('Group',blank=True, null=True)
    
    first_name = models.CharField(max_length=255,blank=True)
    last_name = models.CharField(max_length=255,blank=True)
    location = models.CharField(max_length=255,blank=True)
    password = models.CharField(max_length=255, default=make_password(''))
    last_login = models.CharField(max_length=255, null=True)

    panels = Contact.panels + [
        FieldPanel('first_name'),
        FieldPanel('last_name'),
        FieldPanel('location'),
        FieldPanel('group', widget= CheckboxSelectMultiple),
    ]
    