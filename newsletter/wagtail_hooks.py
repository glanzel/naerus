from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register, ModelAdminGroup

from birdsong.options import CampaignAdmin

from .models import ExtendedContact, SaleCampaign, ACampaign, Group
from birdsong.models import Campaign, Contact
from .filter import ContactFilter
from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings



class TheCampaignAdmin(CampaignAdmin):
    campaign = ACampaign
    menu_label = 'Email'
    menu_icon = 'mail'
    menu_order = 200
    contact_class = ExtendedContact
    contact_filter_class = ContactFilter

class ContactAdmin(ModelAdmin):
    model = ExtendedContact
    menu_label = 'Contacts'
    menu_icon = 'user'
    list_display = ('email', 'first_name', 'last_name', 'show_groups')
    search_fields = ('email', 'first_name', 'last_name' )

    def show_groups(self, obj):
        return " | ".join([a.name for a in obj.group.all()])

class GroupAdmin(ModelAdmin):
    model = Group
    menu_label = 'Groups'
    list_display = ("name",)

@modeladmin_register
class NewsletterGroup(ModelAdminGroup):
    menu_label = 'Newsletter'
    menu_icon = 'mail'
    items = (TheCampaignAdmin, ContactAdmin, GroupAdmin)


# Link Handlers for absolute Urls

from django.utils.html import escape
from wagtail.core import hooks
from wagtail.core.rich_text import LinkHandler
from wagtail.core.rich_text.pages import PageLinkHandler 
from wagtail.documents.rich_text import DocumentLinkHandler
from wagtail.core.models import Site
from django.core.exceptions import ObjectDoesNotExist



# moved to birdsong editor

#===============================================================================
# class AbsoluteLink(object):
#     @classmethod
#     def getAbsoluteLink(cls, page):
#         try:
#             print("getAbsoluteLink")
#             root_url = Site.objects.get(is_default_site=True).root_url
#             page_url = page.url
#             full_url = f"{root_url}{page_url}"
#             return f'<a href="{escape(full_url)}">'
#         except (ObjectDoesNotExist, KeyError):
#             return "<a>"
# 
# class PageAbsoluteLinkHandler(PageLinkHandler):
#     identifier = "page"
#     @classmethod
#     def expand_db_attributes(cls, attrs):
#         try:
#             page = cls.get_instance(attrs)
#             print(cls.__dict__)
#             
#             return f'<a href="{escape(page.full_url)}">'
#         except (ObjectDoesNotExist, KeyError):
#             return "<a>"
# 
# class DocumentAbsoluteLinkHandler(DocumentLinkHandler):
#     identifier = "document"
#     @classmethod
#     def expand_db_attributes(cls, attrs):
#         try:
#             document = cls.get_instance(attrs)
#             root_url = Site.objects.get(is_default_site=True).root_url
#             full_url = f"{root_url}{document.url}"
#             return f'<a href="{escape(full_url)}">'
#         except (ObjectDoesNotExist, KeyError):
#             return "<a>"
#===============================================================================

#@hooks.register('register_rich_text_features')
#def register_link_handler(features):
    #print(features.__dict__)
#    features.register_link_type(DocumentAbsoluteLinkHandler)
#    features.register_link_type(PageAbsoluteLinkHandler)
