# Generated by Django 3.1.3 on 2020-12-17 23:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20201212_0050'),
    ]

    operations = [
        migrations.AddField(
            model_name='blogpage',
            name='teaser',
            field=models.CharField(blank=True, max_length=300),
        ),
    ]
