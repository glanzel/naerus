# Generated by Django 3.1.3 on 2020-12-01 23:25

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20200105_1611'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpage',
            name='body',
            field=wagtail.core.fields.StreamField([('content_image', wagtail.images.blocks.ImageChooserBlock(template='box1_image.html')), ('w2_content_paragraph', wagtail.core.blocks.RichTextBlock())], blank=True, verbose_name='body'),
        ),
    ]
